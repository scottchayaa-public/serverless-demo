# Serverless Demo

# Install serverless tool

```sh
npm install serverless -g
```

# Deploy

```sh
# list configure
aws configure list

# deploy to aws lambda
serverless deploy -v

# test lambda function with params
serverless invoke -f [function] -d '{"key3": "value3","key2": "value2","key1": "value1"}'

# print logs from cloudwatch
serverless logs -f [function] 
# tail logs continutely
serverless logs -f [function] -t

```


# Remove serverless

```
serverless remove
```